﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skaiciuotuvas
{
    public partial class Form1 : Form
    {
        private static double domuo=0;
        private static string zenklas="";
        private static bool ope = false;

        public Form1()
        {
            InitializeComponent();
        }
       

        private void button17_Click(object sender, EventArgs e)
        {
            //PLIUS
            if (Ekranas.Text != "")
            {
                domuo = Convert.ToDouble(Ekranas.Text);
                // Ekranas.Text = string.Empty; // "";
                zenklas = "+";
                ope = true;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //dig0
           
                I_Ekrana('0');
               
         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Isvalyti
            Ekranas.Text = "";
            domuo = 0;
            zenklas = "";
            ope = false;
        }

        private void dig1_Click(object sender, EventArgs e)
        {
            I_Ekrana('1');

        }

        private void dig2_Click(object sender, EventArgs e)
        {
            I_Ekrana('2');

        }

        private void dig3_Click(object sender, EventArgs e)
        {
            I_Ekrana('3');


        }

        private void dig4_Click(object sender, EventArgs e)
        {
            I_Ekrana('4');


        }

        private void dig5_Click(object sender, EventArgs e)
        {
            I_Ekrana('5');


        }

        private void dig6_Click(object sender, EventArgs e)
        {
            I_Ekrana('6');


        }

        private void dig7_Click(object sender, EventArgs e)
        {
            I_Ekrana('7');


        }

        private void dig8_Click(object sender, EventArgs e)
        {
            I_Ekrana('8');


        }

        private void dig9_Click(object sender, EventArgs e)
        {
            I_Ekrana('9');


        }

        private void point_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 6)
            {
                int count = Ekranas.Text.Split(',').Length - 1;
                if (count == 0)
                {
                    Ekranas.Text += ",";
                }
            }

        }

        private void PliusMinus_Click(object sender, EventArgs e)
        {
            
             if (Ekranas.Text.Length != 0)
             {
                 if (Ekranas.Text.Split('-').Length - 1 == 0)
                 {
                     Ekranas.Text = '-' + Ekranas.Text;
                 }
                 else
                 {
                     string a = Ekranas.Text.Remove(0,1);
                     Ekranas.Text = a;
                 }
             }
             
             /*
            if (Ekranas.Text != "")
            {
                Ekranas.Text = Convert.ToString(Convert.ToDouble(Ekranas.Text) * -1);
            }
            */
        }

        private void Minus_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text != "")
            {
                domuo = Convert.ToDouble(Ekranas.Text);
                zenklas = "-";
                ope = true;
            }
        }

        private void Daugyba_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text != "")
            {
                domuo = Convert.ToDouble(Ekranas.Text);
                zenklas = "*";
                ope = true;
            }
        }

        private void Dalyba_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text != "")
            {
                domuo = Convert.ToDouble(Ekranas.Text);
                zenklas = "/";
                ope = true;
            }
        }

        private void Procentai_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text != "")
            {
                domuo = Convert.ToDouble(Ekranas.Text);
                zenklas = "%";
                ope = true;
            }
        }

        private void Grizk_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length != 0)
            {
                Ekranas.Text = Ekranas.Text.Remove(Ekranas.Text.Length - 1);
            }
        }

        private void Lygu_Click(object sender, EventArgs e)
        {
            if (zenklas == "+")
            {
                Ekranas.Text = Convert.ToString(domuo + Convert.ToDouble(Ekranas.Text));
                zenklas = "";
                
            }
            if (zenklas == "-")
            {
                Ekranas.Text = Convert.ToString(domuo - Convert.ToDouble(Ekranas.Text));
                zenklas = "";
                
            }
            if (zenklas == "*")
            {
                Ekranas.Text = Convert.ToString(domuo * Convert.ToDouble(Ekranas.Text));
                zenklas = "";
                
            }
            if (zenklas == "/")
            {
                if (Ekranas.Text != "0")
                {
                    Ekranas.Text = Convert.ToString(domuo / Convert.ToDouble(Ekranas.Text));
                    zenklas = "";
                }
                else
                {
                    Ekranas.Text = "Ot tai matematikas!!!";
                }
            }
            if (zenklas == "%")
            {
                Ekranas.Text = Convert.ToString(domuo / 100 * Convert.ToDouble(Ekranas.Text));
                zenklas = "";
                
            }
            zenklas = "&";

        }

        private void I_Ekrana(char ch)
        {
            if (zenklas == "&")
            {
                Ekranas.Text = "";
                zenklas = "";
            }
            if (zenklas != "" && ope == true)
            {
                Ekranas.Text = "";
                ope = false;
            }
            if (Ekranas.Text == "0")
            {
                Ekranas.Text = "";
            }
            if (Ekranas.Text.Length < 8)
            {
              Ekranas.Text += ch;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //Saknis
            if (Ekranas.Text != "")
            {
                Double sk = Convert.ToDouble(Ekranas.Text);
                Ekranas.Text = Convert.ToString((double)Math.Sqrt(sk));
                zenklas = "&";
            }

        }
    }
}

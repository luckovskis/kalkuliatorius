﻿namespace Skaiciuotuvas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.Isvalyti = new System.Windows.Forms.Button();
            this.dig1 = new System.Windows.Forms.Button();
            this.Grizk = new System.Windows.Forms.Button();
            this.dig2 = new System.Windows.Forms.Button();
            this.dig4 = new System.Windows.Forms.Button();
            this.dig7 = new System.Windows.Forms.Button();
            this.point = new System.Windows.Forms.Button();
            this.dig8 = new System.Windows.Forms.Button();
            this.dig5 = new System.Windows.Forms.Button();
            this.dig0 = new System.Windows.Forms.Button();
            this.PliusMinus = new System.Windows.Forms.Button();
            this.dig9 = new System.Windows.Forms.Button();
            this.dig6 = new System.Windows.Forms.Button();
            this.dig3 = new System.Windows.Forms.Button();
            this.Procentai = new System.Windows.Forms.Button();
            this.Lygu = new System.Windows.Forms.Button();
            this.Plius = new System.Windows.Forms.Button();
            this.Minus = new System.Windows.Forms.Button();
            this.Daugyba = new System.Windows.Forms.Button();
            this.Dalyba = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Enabled = false;
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Ekranas.Location = new System.Drawing.Point(27, 34);
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(318, 38);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Isvalyti
            // 
            this.Isvalyti.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Isvalyti.Location = new System.Drawing.Point(27, 103);
            this.Isvalyti.Name = "Isvalyti";
            this.Isvalyti.Size = new System.Drawing.Size(46, 54);
            this.Isvalyti.TabIndex = 1;
            this.Isvalyti.Text = "C";
            this.Isvalyti.UseVisualStyleBackColor = true;
            this.Isvalyti.Click += new System.EventHandler(this.button1_Click);
            // 
            // dig1
            // 
            this.dig1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig1.Location = new System.Drawing.Point(27, 171);
            this.dig1.Name = "dig1";
            this.dig1.Size = new System.Drawing.Size(75, 59);
            this.dig1.TabIndex = 2;
            this.dig1.Text = "1";
            this.dig1.UseVisualStyleBackColor = true;
            this.dig1.Click += new System.EventHandler(this.dig1_Click);
            // 
            // Grizk
            // 
            this.Grizk.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Grizk.Location = new System.Drawing.Point(79, 103);
            this.Grizk.Name = "Grizk";
            this.Grizk.Size = new System.Drawing.Size(53, 54);
            this.Grizk.TabIndex = 3;
            this.Grizk.Text = "←";
            this.Grizk.UseVisualStyleBackColor = true;
            this.Grizk.Click += new System.EventHandler(this.Grizk_Click);
            // 
            // dig2
            // 
            this.dig2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig2.Location = new System.Drawing.Point(108, 171);
            this.dig2.Name = "dig2";
            this.dig2.Size = new System.Drawing.Size(75, 58);
            this.dig2.TabIndex = 4;
            this.dig2.Text = "2";
            this.dig2.UseVisualStyleBackColor = true;
            this.dig2.Click += new System.EventHandler(this.dig2_Click);
            // 
            // dig4
            // 
            this.dig4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig4.Location = new System.Drawing.Point(27, 236);
            this.dig4.Name = "dig4";
            this.dig4.Size = new System.Drawing.Size(75, 55);
            this.dig4.TabIndex = 5;
            this.dig4.Text = "4";
            this.dig4.UseVisualStyleBackColor = true;
            this.dig4.Click += new System.EventHandler(this.dig4_Click);
            // 
            // dig7
            // 
            this.dig7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig7.Location = new System.Drawing.Point(27, 297);
            this.dig7.Name = "dig7";
            this.dig7.Size = new System.Drawing.Size(75, 58);
            this.dig7.TabIndex = 6;
            this.dig7.Text = "7";
            this.dig7.UseVisualStyleBackColor = true;
            this.dig7.Click += new System.EventHandler(this.dig7_Click);
            // 
            // point
            // 
            this.point.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.point.Location = new System.Drawing.Point(27, 361);
            this.point.Name = "point";
            this.point.Size = new System.Drawing.Size(75, 52);
            this.point.TabIndex = 7;
            this.point.Text = ".";
            this.point.UseVisualStyleBackColor = true;
            this.point.Click += new System.EventHandler(this.point_Click);
            // 
            // dig8
            // 
            this.dig8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig8.Location = new System.Drawing.Point(108, 297);
            this.dig8.Name = "dig8";
            this.dig8.Size = new System.Drawing.Size(75, 58);
            this.dig8.TabIndex = 8;
            this.dig8.Text = "8";
            this.dig8.UseVisualStyleBackColor = true;
            this.dig8.Click += new System.EventHandler(this.dig8_Click);
            // 
            // dig5
            // 
            this.dig5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig5.Location = new System.Drawing.Point(108, 236);
            this.dig5.Name = "dig5";
            this.dig5.Size = new System.Drawing.Size(75, 55);
            this.dig5.TabIndex = 9;
            this.dig5.Text = "5";
            this.dig5.UseVisualStyleBackColor = true;
            this.dig5.Click += new System.EventHandler(this.dig5_Click);
            // 
            // dig0
            // 
            this.dig0.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig0.Location = new System.Drawing.Point(108, 361);
            this.dig0.Name = "dig0";
            this.dig0.Size = new System.Drawing.Size(75, 52);
            this.dig0.TabIndex = 10;
            this.dig0.Text = "0";
            this.dig0.UseVisualStyleBackColor = true;
            this.dig0.Click += new System.EventHandler(this.button10_Click);
            // 
            // PliusMinus
            // 
            this.PliusMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PliusMinus.Location = new System.Drawing.Point(189, 361);
            this.PliusMinus.Name = "PliusMinus";
            this.PliusMinus.Size = new System.Drawing.Size(60, 52);
            this.PliusMinus.TabIndex = 11;
            this.PliusMinus.Text = "+/-";
            this.PliusMinus.UseVisualStyleBackColor = true;
            this.PliusMinus.Click += new System.EventHandler(this.PliusMinus_Click);
            // 
            // dig9
            // 
            this.dig9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig9.Location = new System.Drawing.Point(189, 297);
            this.dig9.Name = "dig9";
            this.dig9.Size = new System.Drawing.Size(75, 58);
            this.dig9.TabIndex = 12;
            this.dig9.Text = "9";
            this.dig9.UseVisualStyleBackColor = true;
            this.dig9.Click += new System.EventHandler(this.dig9_Click);
            // 
            // dig6
            // 
            this.dig6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig6.Location = new System.Drawing.Point(189, 233);
            this.dig6.Name = "dig6";
            this.dig6.Size = new System.Drawing.Size(75, 58);
            this.dig6.TabIndex = 13;
            this.dig6.Text = "6";
            this.dig6.UseVisualStyleBackColor = true;
            this.dig6.Click += new System.EventHandler(this.dig6_Click);
            // 
            // dig3
            // 
            this.dig3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dig3.Location = new System.Drawing.Point(189, 171);
            this.dig3.Name = "dig3";
            this.dig3.Size = new System.Drawing.Size(75, 56);
            this.dig3.TabIndex = 14;
            this.dig3.Text = "3";
            this.dig3.UseVisualStyleBackColor = true;
            this.dig3.Click += new System.EventHandler(this.dig3_Click);
            // 
            // Procentai
            // 
            this.Procentai.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Procentai.Location = new System.Drawing.Point(201, 103);
            this.Procentai.Name = "Procentai";
            this.Procentai.Size = new System.Drawing.Size(63, 58);
            this.Procentai.TabIndex = 15;
            this.Procentai.Text = "%";
            this.Procentai.UseVisualStyleBackColor = true;
            this.Procentai.Click += new System.EventHandler(this.Procentai_Click);
            // 
            // Lygu
            // 
            this.Lygu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Lygu.Location = new System.Drawing.Point(255, 361);
            this.Lygu.Name = "Lygu";
            this.Lygu.Size = new System.Drawing.Size(90, 52);
            this.Lygu.TabIndex = 16;
            this.Lygu.Text = "=";
            this.Lygu.UseVisualStyleBackColor = true;
            this.Lygu.Click += new System.EventHandler(this.Lygu_Click);
            // 
            // Plius
            // 
            this.Plius.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Plius.Location = new System.Drawing.Point(270, 297);
            this.Plius.Name = "Plius";
            this.Plius.Size = new System.Drawing.Size(75, 58);
            this.Plius.TabIndex = 17;
            this.Plius.Text = "+";
            this.Plius.UseVisualStyleBackColor = true;
            this.Plius.Click += new System.EventHandler(this.button17_Click);
            // 
            // Minus
            // 
            this.Minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Minus.Location = new System.Drawing.Point(270, 233);
            this.Minus.Name = "Minus";
            this.Minus.Size = new System.Drawing.Size(75, 58);
            this.Minus.TabIndex = 18;
            this.Minus.Text = "-";
            this.Minus.UseVisualStyleBackColor = true;
            this.Minus.Click += new System.EventHandler(this.Minus_Click);
            // 
            // Daugyba
            // 
            this.Daugyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Daugyba.Location = new System.Drawing.Point(270, 171);
            this.Daugyba.Name = "Daugyba";
            this.Daugyba.Size = new System.Drawing.Size(75, 58);
            this.Daugyba.TabIndex = 19;
            this.Daugyba.Text = "x";
            this.Daugyba.UseVisualStyleBackColor = true;
            this.Daugyba.Click += new System.EventHandler(this.Daugyba_Click);
            // 
            // Dalyba
            // 
            this.Dalyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Dalyba.Location = new System.Drawing.Point(270, 103);
            this.Dalyba.Name = "Dalyba";
            this.Dalyba.Size = new System.Drawing.Size(75, 58);
            this.Dalyba.TabIndex = 20;
            this.Dalyba.Text = "/";
            this.Dalyba.UseVisualStyleBackColor = true;
            this.Dalyba.Click += new System.EventHandler(this.Dalyba_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(138, 103);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 54);
            this.button1.TabIndex = 21;
            this.button1.Text = "√";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 447);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Dalyba);
            this.Controls.Add(this.Daugyba);
            this.Controls.Add(this.Minus);
            this.Controls.Add(this.Plius);
            this.Controls.Add(this.Lygu);
            this.Controls.Add(this.Procentai);
            this.Controls.Add(this.dig3);
            this.Controls.Add(this.dig6);
            this.Controls.Add(this.dig9);
            this.Controls.Add(this.PliusMinus);
            this.Controls.Add(this.dig0);
            this.Controls.Add(this.dig5);
            this.Controls.Add(this.dig8);
            this.Controls.Add(this.point);
            this.Controls.Add(this.dig7);
            this.Controls.Add(this.dig4);
            this.Controls.Add(this.dig2);
            this.Controls.Add(this.Grizk);
            this.Controls.Add(this.dig1);
            this.Controls.Add(this.Isvalyti);
            this.Controls.Add(this.Ekranas);
            this.Name = "Form1";
            this.Text = "Skaičiuotuvas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button Isvalyti;
        private System.Windows.Forms.Button dig1;
        private System.Windows.Forms.Button Grizk;
        private System.Windows.Forms.Button dig2;
        private System.Windows.Forms.Button dig4;
        private System.Windows.Forms.Button dig7;
        private System.Windows.Forms.Button point;
        private System.Windows.Forms.Button dig8;
        private System.Windows.Forms.Button dig5;
        private System.Windows.Forms.Button dig0;
        private System.Windows.Forms.Button PliusMinus;
        private System.Windows.Forms.Button dig9;
        private System.Windows.Forms.Button dig6;
        private System.Windows.Forms.Button dig3;
        private System.Windows.Forms.Button Procentai;
        private System.Windows.Forms.Button Lygu;
        private System.Windows.Forms.Button Plius;
        private System.Windows.Forms.Button Minus;
        private System.Windows.Forms.Button Daugyba;
        private System.Windows.Forms.Button Dalyba;
        private System.Windows.Forms.Button button1;
    }
}

